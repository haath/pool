package models;

import pool.Poolable;


class PoolableItem implements Poolable
{
    public var x: Float;


    @:allow(pool.Pool)
    @:allow(tests.TestPoolable)
    private function new(x: Float)
    {
        this.x = x;
    }
}
