package tests;

import models.PoolableExtends;
import pool.*;
import utest.Assert;
import utest.ITest;
import models.PoolableItem;


class TestPoolable implements ITest
{
    public function new() { }


    /**
        Basic test that the link autobuild macro works correctly.
    **/
    function testPoolableLink()
    {
        var p1: PoolableItem = new PoolableItem(1);
        var p2: PoolableItem = new PoolableItem(1);

        p1._pool_next = p2;

        Assert.equals(p1._pool_next, p2);
    }


    /**
        Tests that the reset() method is generated correctly.
    **/
    function testPoolableReset()
    {
        var p1: PoolableItem = new PoolableItem(1);

        Assert.floatEquals(1, p1.x);

        p1._pool_reset_models_PoolableItem(2);

        Assert.floatEquals(2, p1.x);
    }


    /**
        Tests that a class which extends a poolable type gets generate correctly.
    **/
    function testPoolableExtends()
    {
        var p: PoolableExtends = new PoolableExtends(1, 2);

        Assert.floatEquals(1, p.x);
        Assert.floatEquals(2, p.y);

        p._pool_reset_models_PoolableExtends(5, 6);

        Assert.floatEquals(5, p.x);
        Assert.floatEquals(6, p.y);
    }
}
