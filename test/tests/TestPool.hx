package tests;

import pool.*;
import utest.Assert;
import utest.ITest;
import models.*;
using pool.Pool;


class TestPool implements ITest
{
    var penis: Int = 5;
    public function new() { }


    function testGet()
    {
        var pool: MyPool = new MyPool();
        pool.testCoverage();

        var p1: PoolableItem = pool.getPoolableItem(35.6);
        var p2: PoolableItem = pool.getPoolableItem(8);

        Assert.floatEquals(35.6, p1.x);
        Assert.floatEquals(8, p2.x);
        Assert.notEquals(p1, p2);
    }


    function testPutGet()
    {
        var pool: MyPool = new MyPool();

        var p1: PoolableItem = pool.getPoolableItem(35.6);

        pool.putPoolableItem(p1);

        var p2: PoolableItem = pool.getPoolableItem(8);
        var p3: PoolableItem = pool.getPoolableItem(9);

        Assert.floatEquals(8, p2.x);
        Assert.floatEquals(9, p3.x);
        Assert.equals(p1, p2);
        Assert.notEquals(p3, p2);

        pool.putPoolableItem(p1);
        pool.putPoolableItem(p3);

        Assert.equals(p3, pool.getPoolableItem(0));
        Assert.equals(p1, pool.getPoolableItem(0));
    }


    function testStaticPool()
    {
        var p1: PoolableItem = MyStaticPool.getPoolableItem(35.6);

        MyStaticPool.putPoolableItem(p1);

        var p2: PoolableItem = MyStaticPool.getPoolableItem(8);
        var p3: PoolableItem = MyStaticPool.getPoolableItem(9);

        Assert.floatEquals(8, p2.x);
        Assert.floatEquals(9, p3.x);
        Assert.equals(p1, p2);
        Assert.notEquals(p3, p2);

        MyStaticPool.putPoolableItem(p1);
        MyStaticPool.putPoolableItem(p3);

        Assert.equals(p3, MyStaticPool.getPoolableItem(0));
        Assert.equals(p1, MyStaticPool.getPoolableItem(0));

    }
}
